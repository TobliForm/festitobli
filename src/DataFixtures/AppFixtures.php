<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Category;
use App\Entity\Festival;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private UserPasswordHasherInterface $encoder)
    {
    
    }
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $user = new User;

        $user->setEmail('salif@example.com')
             ->setFirstName('Salif')
             ->setLastName('Coulibaly')
             ->setPhoneNumber('73243411')
             ->setRoles(['ROLE_ADMIN']);

        $password = $this->encoder->hashPassword($user, 'password');

        $user->setPassword($password);
        
        $manager->persist($user);

        /*for ($i=0; $i < 5; $i++) { 

            $category = new Category;

            $category->setDescription($faker->words(2, true));
                      
            $manager->persist($category);

            for ($j=0; $j < 2; $j++) { 


                $duration = mt_rand(3, 10);

                $festival = new Festival;

                $festival->setTitle($faker->words(3, true))
                         ->setSlug($faker->slug(3))
                         ->setDescription($faker->text(350))
                         ->setLocation($faker->words(15, true))
                         ->setCapacity($faker->numberBetween(50, 100, 200))
                         ->setPrice($faker->numberBetween(0, 25000, 50000, 100000, 200000, 300000, 400000, 500000, 1000000))
                         ->setImageName($faker->imageUrl(250, 250))
                         ->setNberDays($duration)
                         ->addCategory($category)
                         ->setUser($user);
                
                         $manager->persist($festival);
            }

            for ($k=0; $k < 2; $k++) { 
                
                $blogpost = new Blogpost;

                $blogpost->setTitle($faker->words(3, true))
                         ->setSlug($faker->slug(3))
                         ->setContent($faker->text(350))
                         ->setImageName($faker->imageUrl(250, 250))
                         ->addCategory($category)
                         ->setUser($user);
                
                         $manager->persist($blogpost);
            }
        }*/

        $manager->flush();
    }
}
