<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Festival;
use App\Form\CommentFormType;
use App\Service\CommentService;
use App\Repository\CommentRepository;
use App\Repository\FestivalRepository;
use App\Repository\InscriptionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FestivalsController extends AbstractController
{
    #[Route('/festivals', name: 'app_festivals_index', methods: ['GET'])]
    public function index(FestivalRepository $festivalRepo, Request $request, PaginatorInterface $paginator): Response
    {
        $data = $festivalRepo->findUpcoming();

        $festivals = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1), /*page number*/
            Festival::NBRE_ELE_PAR_PAGE /*limit per page*/
        );


        return $this->render('festivals/index.html.twig', [
            'festivals' => $festivals,
        ]);
    }

    #[Route('/festivals/{slug}', name: 'app_festivals_show', methods: ['GET', 'POST'])]
    public function show(Festival $festival, InscriptionRepository $inscriptionRepo, Request $request, CommentService $commentService, CommentRepository $commentRepo): Response
    {
        $isInscripted = (bool) $inscriptionRepo->findOneBy([
            'user' => $this->getUser(),
            'festival' => $festival,
        ]);

        $comment = new Comment;
        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $commentService->persistComment($comment, $festival, null);

            return $this->redirectToRoute('app_festivals_show', ['slug' => $festival->getSlug()]);
        }

        return $this->render('festivals/show.html.twig', [
            'festival' => $festival,
            'isInscripted' => $isInscripted,
            'form' => $form->createView(),
            'comments' => $commentRepo->findComment($festival),
        ]);
    }
}
