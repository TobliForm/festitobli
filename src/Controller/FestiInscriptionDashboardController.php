<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Festival;
use App\Entity\Inscription;
use App\Repository\InscriptionRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FestiInscriptionDashboardController extends AbstractController
{
    #[Route('/dashboard', name: 'app_dashboard', methods: ['GET'])]
    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(InscriptionRepository $inscriptionRepo): Response
    {
        if (! $this->getUser()) {
            $this->addFlash('info', "Vous devez d'abord vous connecter!");
            return $this->redirectToRoute('app_login');
         }

         if (! $this->getUser()->isVerified()) {
            $this->addFlash('info', 'Vous devez avoir un compte actif!');
            return $this->redirectToRoute('app_home');
         }
         
        $userInscriptions = $inscriptionRepo->findByUserInscription();
        return $this->render('dashboard/index.html.twig', [
            'userInscriptions' => $userInscriptions,
        ]);
    }
}
