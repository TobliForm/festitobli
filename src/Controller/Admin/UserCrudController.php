<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\RoleType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstName', 'Prénom'),
            TextField::new('lastName', 'Nom'),
            EmailField::new('email', 'Email'),
            TextField::new('password', 'Mot de passe')->hideOnIndex(),
            TextField::new('phoneNumber', 'Numéro de téléphone'),
            BooleanField::new('isVerified', 'Activer le compte'),
            ChoiceField::new('roles', 'Roles')
                ->setChoices([ 
                    'ADMIN' => 'ROLE_ADMIN',
                    'USER' => 'ROLE_USER',
                ])->allowMultipleChoices(false)
                  ->renderExpanded(true)
                  ->setFormType(RoleType::class)
        ];
    }

    public function configureCrud(Crud $Crud): Crud
    {
        return $Crud
            ->setDefaultSort(['createdAt' => 'DESC']);
    }   
    
}
