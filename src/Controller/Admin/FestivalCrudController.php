<?php

namespace App\Controller\Admin;

use App\Entity\Festival;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class FestivalCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Festival::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Titre'),
            SlugField::new('slug')->setTargetFieldName('title')->hideOnIndex(),
            TextareaField::new('description')->hideOnIndex(),
            TextField::new('location', 'Lieu'),
            IntegerField::new('capacity', 'Capacité'),
            IntegerField::new('price', 'Prix'),
            AssociationField::new('category', 'Tags'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenCreating(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenUpdating(),
            ImageField::new('imageName', 'Image')->setBasePath('/uploads/festivals/')->onlyOnIndex(),
            DateField::new('startsAt', 'Date début'),
            DateField::new('endAt', 'Date fin'),
            IntegerField::new('nberDays'),
        ];
    }

    public function configureCrud(Crud $Crud): Crud
    {
        return $Crud
            ->setDefaultSort(['createdAt' => 'DESC']);
    }   
    
}
