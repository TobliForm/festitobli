<?php

namespace App\Controller\Admin;

use App\Entity\Blogpost;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BlogpostCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Blogpost::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
           TextField::new('title', 'Titre'),
           SlugField::new('slug')->setTargetFieldName('title')->hideOnIndex(),
           TextareaField::new('content', 'Contenu')->hideOnIndex(),
           AssociationField::new('category', 'Tags')->hideOnIndex(),
           TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenCreating(),
           TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenUpdating(),
           ImageField::new('imageName', 'Image')->setBasePath('/uploads/blogposts/')->onlyOnIndex(),
        ];
    }

    public function configureCrud(Crud $Crud): Crud
    {
        return $Crud
            ->setDefaultSort(['createdAt' => 'DESC']);
    }   

    
}
