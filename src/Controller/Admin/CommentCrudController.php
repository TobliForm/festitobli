<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('festival', 'Festivals'),
            AssociationField::new('blogpost', 'Blogposts'),
            TextareaField::new('content', 'Contenu')->hideOnIndex(),
            TextField::new('user', 'Auteur')->onlyOnIndex(),
            BooleanField::new('isPublised', 'Publier'),
        ];
    }

    public function configureCrud(Crud $Crud): Crud
    {
        return $Crud
            ->setDefaultSort(['createdAt' => 'DESC']);
    }   
    
}
