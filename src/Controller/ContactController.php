<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use App\Notification\ContactNotification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'app_contact', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em, ContactNotification $contactNotification): Response
    {
        if (! $this->getUser()) {
            $this->addFlash('info', "Vous devez d'abord vous connecter!");
            return $this->redirectToRoute('app_login');
         }
         
        $contact = new Contact;
        $user = $this->getUser();

        $form = $this->createForm(ContactFormType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setUser($user);
            $em->persist($contact);
            $em->flush();

            $contactNotification->notify($contact);

            $this->addFlash('success', 'Votre mail a été envoyé avec succès. Nous vous répondrons dans le plus bref délai!');
            return $this->redirectToRoute('app_home');
        }
        return $this->render('contact/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
