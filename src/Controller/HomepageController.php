<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use App\Repository\FestivalRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomepageController extends AbstractController
{
    #[Route('/', name: 'app_home', methods: ['GET'])]
    public function index(FestivalRepository $festiRepo, BlogpostRepository $blogpostRepo): Response
    {
        $festivals = $festiRepo->findUpcoming(3);
        $blogposts = $blogpostRepo->findLastBlog();
        return $this->render('homepage/index.html.twig', compact('festivals', 'blogposts'));
    }
}
