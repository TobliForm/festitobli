<?php

namespace App\Controller;

use App\Entity\Festival;
use App\Entity\Inscription;
use App\Form\FestivalIncriptionFormType;
use Doctrine\ORM\EntityManagerInterface;
use App\Notification\InscriptionNotification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FestivalInscriptionController extends AbstractController
{
    #[Route('/festival/inscription/{slug}', name: 'festival_inscription_create', methods: ['GET', 'POST'])]
    public function create(Request $request, Festival $festival, EntityManagerInterface $em, InscriptionNotification $inscriptionNotification): Response
    {
        if (! $this->getUser()) {
            $this->addFlash('info', "Vous devez d'abord vous connecter!");
            return $this->redirectToRoute('app_login');
         }

         if (! $this->getUser()->isVerified()) {
            $this->addFlash('info', 'Vous devez avoir un compte actif!');
            return $this->redirectToRoute('app_home');
         }
         
        $inscription = New Inscription;

        $user = $this->getUser();

        $form = $this->createForm(FestivalIncriptionFormType::class, $inscription);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inscription->setUser($user)
                        ->setFestival($festival)
                        ->setIsSend(True);
            
            $em->persist($inscription);
            $em->flush(); 

            $inscriptionNotification->notify($inscription);
            $this->addFlash('info', 'Un mail vous a été envoyé pour vous notifier votre inscription au ' . $festival->getTitle());
            return $this->redirectToRoute('app_home');
        }


        return $this->render('festival_inscription/create.html.twig', [
            'form' => $form->createView(),
            'festival' => $festival,
            'user' => $user
        ]);
    }
}
