<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Blogpost;
use App\Form\CommentFormType;
use App\Service\CommentService;
use App\Repository\CommentRepository;
use App\Repository\BlogpostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogpostsController extends AbstractController
{
    #[Route('/blogposts', name: 'app_blogposts_index', methods:['GET'])]
    public function index(BlogpostRepository $blogpostRepo, Request $request, PaginatorInterface $paginator): Response
    {
        $data = $blogpostRepo->findBy([], ['createdAt' => 'DESC']);

        $blogposts = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1), /*page number*/
            Blogpost::NBRE_ELE_PAR_PAGE /*limit per page*/
        );
        return $this->render('blogposts/index.html.twig', [
            'blogposts' => $blogposts,
        ]);
    }

    #[Route('/blogposts/{slug}', name: 'app_blogposts_show', methods:['GET', 'POST'])]
    public function sow(Blogpost $blogpost, Request $request, CommentService $commentService, CommentRepository $commentRepo): Response
    {
        /*if (!$blogpost) {
            //throw $this->createNotFoundException('Not found');
            //return $this->redirectToRoute('app_blogposts_show', ['slug' => $blogpost->getSlug()]);
            //return $this->generateUrl('app_blogposts_show', ['slug' => $blogpost->getSlug()]);
        }*/

        $comment = new Comment;
        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $commentService->persistComment($comment, null, $blogpost);

            return $this->redirectToRoute('app_blogposts_show', ['slug' => $blogpost->getSlug()]);
        }
        return $this->render('blogposts/show.html.twig', [
            'blogpost' => $blogpost,
            'form' => $form->createView(),
            'comments' => $commentRepo->findComment($blogpost),
        ]);
    }
}
