<?php

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Blogpost;
use App\Entity\Festival;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentService
{
    private $em;
    private $flash;
    private $user;

    public function __construct(EntityManagerInterface $em, FlashBagInterface $flash, Security $user)
    {
        $this->em = $em;
        $this->flash = $flash;
        $this->user = $user;
    }

    public function persistComment(Comment $comment, Festival $festival = null, Blogpost $blogpost = null): void
    {
        $comment->setisPublised(false)
            ->setFestival($festival)
            ->setBlogpost($blogpost)
            ->setUser($this->user->getUser());
        
        $this->em->persist($comment);
        $this->em->flush();

        $this->flash->add('success', 'Votre commentaire est bien envoyé, merci. Il sera publié après validation');
    }

}
