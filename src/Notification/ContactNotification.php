<?php

namespace App\Notification;

use App\Entity\Contact;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ContactNotification
{
    private $mailer;
    private $parameter;

    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameter)
    {
        $this->mailer = $mailer;
        $this->parameter = $parameter;
    }

    public function notify(Contact $contact)
    {
        $email = (new TemplatedEmail())
            ->from($contact->getUser()->getEmail())
            ->to($this->parameter->get('app.mail_from_address'))
            ->replyTo($contact->getUser()->getEmail())
            ->subject($this->parameter->get('app.name') . ' ' . 'Toujours à votre disposition pour tout renseignement')
            ->htmlTemplate('emails/notifications/contact.html.twig')
            ->context([
                'contact' => $contact,
            ]);

        $this->mailer->send($email);    
    }
}