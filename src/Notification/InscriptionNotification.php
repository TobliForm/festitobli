<?php

namespace App\Notification;

use App\Entity\Inscription;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class InscriptionNotification
{
    private $mailer;
    private $parameter;

    public function __construct(MailerInterface $mailer, ParameterBagInterface $parameter)
    {
        $this->mailer = $mailer;
        $this->parameter = $parameter;
    }

    public function notify(Inscription $inscription)
    {
        $email = (new TemplatedEmail())
            ->from($this->parameter->get('app.mail_from_address'))
            ->to($inscription->getUser()->getEmail())
            ->replyTo($this->parameter->get('app.mail_from_address'))
            ->subject($this->parameter->get('app.name') . ' ' . $inscription->getFestival()->getTitle())
            ->htmlTemplate('emails/notifications/inscription.html.twig')
            ->context([
                'inscription' => $inscription,
            ]);

        $this->mailer->send($email);    
    }
}