<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\InscriptionRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InscriptionRepository::class)
 * @ORM\Table(name="inscriptions")
 * @ORM\HasLifecycleCallbacks
 */
class Inscription
{
    use Timestampable;

    const HOWHEARD_OPTION = [

        'Facebook' => 'Facebook',
        'Twitter' => 'Twitter',
        'Blog Post' => 'Blog Post',
        'Recherche Internet' => 'Recherche Internet',
        'Ami(e)/Collaborateur(rice)' => 'Ami(e)/Collaborateur(rice)',
        'Autre' => 'Autre',
        
    ];
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est requis.")
     * @Assert\Choice(callback="getHowHeardOption", message="La valeur que vous avez sélectionnée n'est pas un choix valide.")
     */
    private $howHeard;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSend;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="inscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Festival::class, inversedBy="inscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $festival;

    public function getHowHeardOption()
    {
        return array_values(self::HOWHEARD_OPTION);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHowHeard(): ?string
    {
        return $this->howHeard;
    }

    public function setHowHeard(string $howHeard): self
    {
        $this->howHeard = $howHeard;

        return $this;
    }

    public function getIsSend(): ?bool
    {
        return $this->isSend;
    }

    public function setIsSend(bool $isSend): self
    {
        $this->isSend = $isSend;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFestival(): ?Festival
    {
        return $this->festival;
    }

    public function setFestival(?Festival $festival): self
    {
        $this->festival = $festival;

        return $this;
    }
}
