<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\FestivalRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FestivalRepository::class)
 * @ORM\Table(name="festivals")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("title", message="Ce titre a déjà été utilisé, veuillez réessayer un autre!")
 * @Vich\Uploadable
 */
class Festival
{
    use Timestampable;
    const NBRE_ELE_PAR_PAGE = 6;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Le champ titre ne peut pas être vide")
     * @Assert\Length(min = 5, minMessage = "Le champ titre doit avoir au minimun {{ limit }} caractères")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le champ description ne peut pas être vide")
     * @Assert\Length(min = 10, minMessage = "Le champ description doit avoir au minimun {{ limit }} caractères")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le champ lieu ne peut pas être vide")
     */
    private $location;

    /**
     * @ORM\Column(type="integer", options={"default": 1})
     * @Assert\NotBlank(message="Le champ capacité ne peut pas être vide")
     * @Assert\Positive
     */
    private $capacity = 1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\PositiveOrZero
     */
    private $price;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="festi_image", fileNameProperty="imageName")
     * @Assert\File(maxSize = "8M", maxSizeMessage="L'image est trop grande la taille est de ({{ size }} {{ suffix }}). La taille maximale doit être de {{ limit }} {{ suffix }}.")
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     * @Assert\NotBlank(message="Le champ Date de début ne peut pas être vide")
     * @Assert\GreaterThanOrEqual("+1 hour")
     */
    private $startsAt;

    /**
     * @ORM\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"})
     * @Assert\NotBlank(message="Le champ Date de fin ne peut pas être vide")
     * @Assert\GreaterThanOrEqual("+1 hour")
     */
    private $endAt;

    /**
     * @ORM\Column(type="integer", options={"default": 1})
     * @Assert\NotBlank(message="Le champ Nombre de jours ne peut pas être vide")
     * @Assert\Positive
     */
    private $nberDays = 1;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="festivals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="festival", fetch="EAGER")
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="festivals")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity=Inscription::class, mappedBy="festival", fetch="EAGER", orphanRemoval=true)
     */
    private $inscriptions;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
    }

    public function isFree(): bool
    {
        return $this->getPrice() == 0 || $this->getPrice() == null;
    }

    public function numberOfPlaces(): int
    {
        return $this->getCapacity() - ($this->getInscriptions()->count());
    }

    public function isSoldOut(): bool
    {
        return $this->numberOfPlaces() == 0;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(?int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setUpdatedAt(new \DateTimeImmutable);
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getStartsAt(): ?\DateTimeImmutable
    {
        return $this->startsAt;
    }

    public function setStartsAt(?\DateTimeImmutable $startsAt): self
    {
        $this->startsAt = $startsAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeImmutable
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeImmutable $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getNberDays(): ?int
    {
        return $this->nberDays;
    }

    public function setNberDays(?int $nberDays): self
    {
        $this->nberDays = $nberDays;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setFestival($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getFestival() === $this) {
                $comment->setFestival(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->category->removeElement($category);

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setFestival($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->removeElement($inscription)) {
            // set the owning side to null (unless already changed)
            if ($inscription->getFestival() === $this) {
                $inscription->setFestival(null);
            }
        }

        return $this;
    }
}
