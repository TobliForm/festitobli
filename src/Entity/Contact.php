<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\ContactRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 * @ORM\Table(name="contacts")
 * @ORM\HasLifecycleCallbacks
 */
class Contact
{
    use Timestampable;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le champ message ne peut pas être vide")
     * @Assert\Length(min = 10, minMessage = "Le champ message doit avoir au minimun {{ limit }} caractères")
     */
    private $message;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotBlank(message="Le champ je ne suis pas un robot ne peut pas être vide")
     * @Assert\EqualTo(value = 13, message="La valeur que vous avez fourni ne correspond pas")
     */
    private $notRobot;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getNotRobot(): ?int
    {
        return $this->notRobot;
    }

    public function setNotRobot(?int $notRobot): self
    {
        $this->notRobot = $notRobot;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
