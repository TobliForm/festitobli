<?php

namespace App\EntityListener;

use App\Entity\Festival;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class FestivalEntityListener
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Festival $festival, LifecycleEventArgs $event)
    {
        $slug = $this->slugger->slug($festival->getTitle());
        $festival->setSlug($slug);
    }

    public function preUpdate(Festival $festival, LifecycleEventArgs $event)
    {
        $slug = $this->slugger->slug($festival->getTitle());
        $festival->setSlug($slug);
    }
}
