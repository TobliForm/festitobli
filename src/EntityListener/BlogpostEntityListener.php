<?php

namespace App\EntityListener;

use App\Entity\Blogpost;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class BlogpostEntityListener
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function prePersist(Blogpost $blogpost, LifecycleEventArgs $event)
    {
        $slug = $this->slugger->slug($blogpost->getTitle());
        $blogpost->setSlug($slug);
    }

    public function preUpdate(Blogpost $blogpost, LifecycleEventArgs $event)
    {
        $slug = $this->slugger->slug($blogpost->getTitle());
        $blogpost->setSlug($slug);
    }
}
