<?php

namespace App\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Entity\Festival;
use Twig\Extension\AbstractExtension;
use Symfony\Component\Routing\RouterInterface;

class AppExtension extends AbstractExtension
{
    public function __construct(private RouterInterface $router)
    {}

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('datetime', [$this, 'formatDateTime']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('format_price', [$this, 'formatPrice'], ['is_safe' => ['html']]),
            new TwigFunction('festivalIsSoldOut', [$this, 'festivalIsSoldOut'], ['is_safe' => ['html']]),
            new TwigFunction('pluralize', [$this, 'pluralize']),
        ];
    }

    public function formatPrice(Festival $festival)
    {
        return $festival->isFree()
            ? '<b><span class="badge rounded-pill bg-primary">Gratuit!</span></b>'
            : '<span class="badge rounded-pill bg-secondary">' . number_format($festival->getPrice(), 0, '', ' ') . ' F CFA</span>';
    }

    public function formatDateTime(\DateTimeInterface $dateTime): string
    {
        return $dateTime->format('d/m/Y');
    }

    /*public function pluralize(int $count, string $singular, ?string $plural = null)
    {
        $plural = $plural ?? $singular . 's';

        $string = $count == 1 || $count == 0 ? $singular : $plural;

        return "$count $string";
    }*/

    public function festivalIsSoldOut(Festival $festival)
    {
        if ($festival->isSoldOut()) {
            return '<i class="bi bi-shield-lock-fill"></i> <span class="badge bg-info">Plus de place disponible!</span>';
        } 
    }
}
