<?php

namespace App\EventSubscriber;

use App\Entity\Blogpost;
use App\Entity\Comment;
use App\Entity\Festival;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class EasyAdminEventSubscriber implements EventSubscriberInterface
{

    public function __construct(private Security $security)
    {
    
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setUser'], 
        ];
    }

    public function setUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (($entity instanceof Blogpost)) {
            $user = $this->security->getUser();
            $entity->setUser($user);
        }
    
        if (($entity instanceof Festival)) {
            $user = $this->security->getUser();
            $entity->setUser($user);
        }

        if (($entity instanceof Comment)) {
            $user = $this->security->getUser();
            $entity->setUser($user);
        }

        return;
    }
}

