<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210627234737 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create blogpost_category table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE blogpost_category (blogpost_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_B9261D4E27F5416E (blogpost_id), INDEX IDX_B9261D4E12469DE2 (category_id), PRIMARY KEY(blogpost_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blogpost_category ADD CONSTRAINT FK_B9261D4E27F5416E FOREIGN KEY (blogpost_id) REFERENCES blogposts (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blogpost_category ADD CONSTRAINT FK_B9261D4E12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE blogpost_category');
    }
}
