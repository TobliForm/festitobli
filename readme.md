# FestiTobli

FestiTobli est une application web qui permet de mettre en valeur la culture malienne, à travers les différents festivals et activités culturelles organisés au Mali tout au long de l'année. Le blog FestiTobli vous permet de voyager au fin fond du Mali pour découvrir des choses insoupçonnées.  

## Environnement de développement

### Pré-requis

* PHP 7.4 
* Composer
* Symfony CLI

### Vous pouvez verifier les pré-requis avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
symfony serve -d
```

